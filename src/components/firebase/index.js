// eslint-disable-next-line no-unused-vars
import "@firebase/storage"
import "@firebase/auth"
import "@firebase/functions"
import "@firebase/firestore"
import firebase from "@firebase/app"

// Initialize Firebase
const config = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
}

let firebaseCache = null

const getFirebase = firebase => {
  if (typeof window !== "undefined") {
    if (firebaseCache) {
      return firebaseCache
    }

    firebaseCache = firebase.initializeApp(config)
    return firebase
  }

  return null
}
//TODO
export default firebase.initializeApp(config)
