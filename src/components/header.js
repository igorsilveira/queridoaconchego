import PropTypes from "prop-types"
import React, { Component } from "react"
import { navigate } from "gatsby"

import TopMenu from "../containers/Menu/TopMenu"
import { withFirebase } from "../components/firebase/context"

class Header extends Component {
  signIn = () => {
    const { firebase } = this.props
    firebase
      .auth()
      .signInWithEmailAndPassword("t@t.com", "123456")
      .then(navigate("/"))
  }

  render() {
    return <TopMenu />
  }
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default withFirebase(Header)
