import React from "react"
import PropTypes from "prop-types"
import { withStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Typography from "@material-ui/core/Typography"

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3, align: "left" }}>
      {props.children}
    </Typography>
  )
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    boxShadow: "0px 10px 31px -16px rgba(0,0,0,0.38)",
    borderRadius: 10,
  },
})

class SimpleTabs extends React.Component {
  state = {
    value: 0,
  }

  handleChange = (event, value) => {
    this.setState({ value })
  }

  render() {
    const { classes, info } = this.props
    const { value } = this.state

    let generalInfo = []

    for (const key in info.general) {
      if (
        info.general.hasOwnProperty(key) &&
        typeof info.general[key] !== "undefined"
      ) {
        const element = info.general[key]
        generalInfo.push(
          <Typography key={key} variant="body1" style={{ lineHeight: 3 }}>
            <span style={{ fontWeight: "bold" }}>{`${key}: `}</span>
            {`${element}`}
          </Typography>
        )
      }
    }

    return (
      <div className={classes.root}>
        <Tabs value={value} onChange={this.handleChange} variant="standard">
          <Tab label="Geral" />
          <Tab label="Sobre" />
          {/* {info.history != "undefined" && <Tab label="História" />} */}
        </Tabs>
        {value === 0 && <TabContainer>{generalInfo}</TabContainer>}
        {value === 1 && <TabContainer>{info.about}</TabContainer>}
        {/* {value === 2 && <TabContainer>{info.history}</TabContainer>} */}
      </div>
    )
  }
}

SimpleTabs.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(SimpleTabs)
