import React from "react"
import PropTypes from "prop-types"
import { withStyles } from "@material-ui/core/styles"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"

const size = 90

const styles = {
  media: {
    height: size,
    width: size,
  },
  dog: {
    // width: "100%",
  },
}

const DogCard = props => {
  const { classes, dog } = props

  return (
    <Grid container spacing={16} className={classes.dog}>
      <Grid item>
        <img className={classes.img} alt="complex" src={dog.imageURL} />
      </Grid>
      <Grid item xs={12} sm container>
        <Grid item container direction="column" spacing={16}>
          <Grid item>
            <Typography gutterBottom variant="h5">
              {dog.name}
            </Typography>
            <Typography gutterBottom variant="subtitle1" color="textSecondary">
              {dog.breed} - {dog.genre}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

DogCard.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(DogCard)
