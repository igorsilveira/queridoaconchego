import React from "react"
import { Link } from "gatsby"

import Card from "@material-ui/core/Card"
import CardActionArea from "@material-ui/core/CardActionArea"
import CardMedia from "@material-ui/core/CardMedia"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"

import { withStyles } from "@material-ui/core/styles"

const styles = theme => ({
  img: {
    width: "100%",
    display: "block",
    height: "auto",
  },
  card: {
    width: "100%",
    background: "none",
    boxShadow: "none",
  },
  columnFields: {
    marginLeft: theme.spacing.unit * 4,
  },
})

const DogColumn = props => {
  const { id, name, breed, genre, url, classes } = props
  return (
    <div>
      <Grid
        container
        item
        direction="column"
        align="left"
        spacing={0}
        className={classes.gridContainer}
      >
        <Card className={classes.card}>
          <CardActionArea>
            <a href={`/amigo?id=${id}`} style={{ textDecoration: "none" }}>
              <CardMedia
                component="img"
                alt={name}
                className={classes.media}
                // height="140"
                image={url}
                title={name}
              />
            </a>
          </CardActionArea>
        </Card>
        <Grid item className={classes.columnFields}>
          <Typography variant="h4" color="primary" gutterBottom>
            {name && name.toUpperCase()}
          </Typography>
          <Typography variant="h6" color="primary" gutterBottom>
            {breed}
          </Typography>
          <Typography variant="body1" color="primary" gutterBottom>
            {genre}
          </Typography>
        </Grid>
      </Grid>
    </div>
  )
}

export default withStyles(styles)(DogColumn)
