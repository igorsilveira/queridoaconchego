import {
  createStore as reduxCreateStore,
  applyMiddleware,
  compose,
} from "redux"

import reducer from "../reducers/index"

const initialState = {
  dogs: [],
}

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose

const enhancer = composeEnhancers(
  applyMiddleware()
  // other store enhancers if any
)
const createStore = () => reduxCreateStore(reducer, initialState, enhancer)

export default createStore
