import { SET_DOGS, ADD_DOG, MODIFY_DOG, REMOVE_DOG } from "../actions/dogs"

const reducer = (state, action) => {
  switch (action.type) {
    case SET_DOGS:
      return Object.assign({}, state, { dogs: action.dogs })
    case ADD_DOG:
      return Object.assign({}, state, {
        dogs: [...state.dogs, action.dog],
      })
    case MODIFY_DOG:
      let oldDogsModify = [...state.dogs]
      const indexModify = oldDogsModify.findIndex(
        dog => dog.id === action.dog.id
      )
      oldDogsModify.splice(indexModify, 1, action.dog)
      return Object.assign({}, state, { dogs: oldDogsModify })
    case REMOVE_DOG:
      let oldDogsRemove = [...state.dogs]
      const indexRemove = oldDogsRemove.findIndex(
        dog => dog.id === action.dog.id
      )
      oldDogsRemove.splice(indexRemove, 1)
      return Object.assign({}, state, { dogs: oldDogsRemove })
    default:
      return state
  }
}

export default reducer
