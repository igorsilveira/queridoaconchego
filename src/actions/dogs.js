export const SET_DOGS = "SET_DOGS"
export const ADD_DOG = "ADD_DOG"
export const MODIFY_DOG = "MODIFY_DOG"
export const REMOVE_DOG = "REMOVE_DOG"

export const setDogs = dogs => ({ type: SET_DOGS, dogs })
export const addDog = dog => ({ type: ADD_DOG, dog })
export const modifyDog = dog => ({ type: MODIFY_DOG, dog })
export const removeDog = dog => ({ type: REMOVE_DOG, dog })
