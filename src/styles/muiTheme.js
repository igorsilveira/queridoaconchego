import { createMuiTheme } from "@material-ui/core/styles"

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#ffffff",
      main: "#ffffff",
      dark: "#cccccc",
      contrastText: "#000000",
    },
    secondary: {
      light: "#796178",
      main: "#4D374C",
      dark: "#241124",
      contrastText: "#ffffff",
    },
    fallback: {
      light: "#dbb286",
      main: "#D29F68",
      dark: "#936f48",
      contrastText: "#ffffff",
    },
  },
  typography: {
    useNextVariants: true,
  },
})

export { theme }
