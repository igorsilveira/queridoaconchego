import React, { Component } from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql, navigate } from "gatsby"
import { connect } from "react-redux"

import Header from "../components/header"
import Container from "../containers/container"
import getFirebase from "../components/firebase"
import FirebaseContext from "../components/firebase/context"
import SEO from "../components/seo"

import ComingSoonPage from "../pages/coming-soon"

import { withStyles } from "@material-ui/core/styles"
import { MuiThemeProvider } from "@material-ui/core/styles"
import { theme } from "../styles/muiTheme"
import { setDogs, addDog, removeDog, modifyDog } from "../actions/dogs"

import "../styles/layout.css"

const dogsRef = "dogs"

const ADDED = "added"
const MODIFIED = "modified"
const REMOVED = "removed"

const mapStateToProps = ({ dogs }) => ({ dogs })

const mapDispatchToProps = dispatch => ({
  setDogs: dogs => dispatch(setDogs(dogs)),
  addDog: dog => dispatch(addDog(dog)),
  removeDog: dog => dispatch(removeDog(dog)),
  modifyDog: dog => dispatch(modifyDog(dog)),
})

const styles = () => ({
  footer: {
    padding: 10,
    color: theme.palette.primary.contrastText,
    textAlign: "center",
  },
})

const Footer = ({ classes }) => (
  <footer className={classes.footer}>
    <Container>
      © {new Date().getFullYear()}, Built with
      {` `}
      <a
        style={{ color: theme.palette.primary.contrastText }}
        href="https://www.gatsbyjs.org"
      >
        Gatsby
      </a>
      {` by `}
      <a
        style={{ color: theme.palette.primary.contrastText }}
        href="https://github.com/igorasilveira"
      >
        Igor Silveira{" "}
      </a>
    </Container>
  </footer>
)

class Layout extends Component {
  constructor(props) {
    super(props)

    this.state = {
      firebase: null,
      authenticated: false,
      loading: true,
      dev: false,
    }
  }

  componentDidMount() {
    const app = import("@firebase/app")
    const auth = import("@firebase/auth")
    const firestore = import("@firebase/firestore")
    const storage = import("@firebase/storage")

    if (
      typeof window !== "undefined" &&
      localStorage.getItem("klaosk") === "1"
    ) {
      this.setState({ dev: true })
    }

    Promise.all([app, auth, firestore, storage]).then(values => {
      const firebase = getFirebase
      //TODO
      // const firebase = getFirebase(values[0])
      this.setState({ firebase })

      // firebase.auth().onAuthStateChanged(user => {
      //   if (!user) {
      //     this.setState({ authenticated: false })
      //   } else {
      //     this.setState({ authenticated: true })
      //   }
      // })

      // this.dogListener = firebase
      //   .firestore()
      //   .collection(dogsRef)
      //   .onSnapshot(snapshot => {
      //     snapshot.docChanges().forEach(change => {
      //       firebase
      //         .storage()
      //         .ref(`dogs/${change.doc.id}.png`)
      //         .getDownloadURL()
      //         .then(url => {
      //           const newDog = {
      //             id: change.doc.id,
      //             ...change.doc.data(),
      //             imageURL: url,
      //           }
      //           switch (change.type) {
      //             case ADDED:
      //               this.props.addDog(newDog)
      //               break
      //             case MODIFIED:
      //               this.props.modifyDog(newDog)
      //               break
      //             case REMOVED:
      //               this.props.removeDog(newDog)
      //               break
      //             default:
      //               break
      //           }
      //         })
      //     })
      //   })
      // .catch(err => console.error("error getting dogs", err))
    })
  }

  // componentWillUnmount = () => {
  //   this.dogListener()
  // }

  render = () => {
    const { classes } = this.props
    const { firebase, authenticated, dev } = this.state

    if (!dev && typeof window !== "undefined") {
      if (window.location.pathname != "/") {
        navigate("/")
      }

      // Return Coming Soon
      return (
        <MuiThemeProvider theme={theme}>
          <ComingSoonPage />
          <Footer classes={classes} />
        </MuiThemeProvider>
      )
    }

    if (!firebase) return null

    return (
      <StaticQuery
        query={query}
        render={data => (
          <MuiThemeProvider theme={theme}>
            <SEO
              title="Querido Aconchego"
              keywords={[
                `animal`,
                `shelter`,
                `querido aconchego`,
                `abrigo`,
                `associação animal`,
                `felgueiras`,
              ]}
            />
            <Header siteTitle={data.site.siteMetadata.title} />
            <main style={{ paddingTop: 0 }}>
              <FirebaseContext.Provider value={firebase}>
                {/* {authenticated ? this.props.children : <p>Sign In</p>} */}
                {this.props.children}
              </FirebaseContext.Provider>
            </main>
            <Footer classes={classes} />
          </MuiThemeProvider>
        )}
      />
    )
  }
}

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Layout))
