import React from "react"
import { withStyles } from "@material-ui/core/styles"
import classNames from "classnames"

//Layout
import Container from "./container"
import Paper from "@material-ui/core/Paper"

const styles = theme => ({
  root: {
    flexGrow: 1,
    position: "relative",
    padding: "80px 0",
  },
  mid: {
    backgroundColor: theme.palette.fallback.main,
  },
})

const DoubleColor = props => {
  const { classes, children, customClass, inverted = false } = props
  return (
    <div className={classes.root}>
      <Container>
        <Paper
          className={classNames(
            classes.mid,
            "halfColor",
            inverted ? "inverted" : "",
            customClass ? customClass : "darkPurple"
          )}
        >
          {children}
        </Paper>
      </Container>
    </div>
  )
}

export default withStyles(styles)(DoubleColor)
