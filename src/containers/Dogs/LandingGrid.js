import React, { Component } from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { withFirebase } from "../../components/firebase/context"

//Layout
import { withStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import CircularProgress from "@material-ui/core/CircularProgress"

import DogCard from "../../components/dogs/DogCard"

const dogsRef = "dogs"

const styles = {
  gridContainer: {
    marginTop: 20,
  },
  paperDog: {
    padding: "20px 60px",
    boxShadow: "0px 25px 41px -32px rgba(0,0,0,0.58)",
  },
}

const DogGrid = props => {
  const { dogs } = props
  return (
    <>
      {dogs.map((dog, index) => (
        <Grid item key={index}>
          <Link to={`/amigo?id=${dog.id}`} style={{ textDecoration: "none" }}>
            <DogCard dog={dog} />
          </Link>
        </Grid>
      ))}
    </>
  )
}

class LandingGrid extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dogs: [],
      loading: true,
    }
  }

  componentDidMount() {
    const { firebase } = this.props
    this.loadDogs = firebase
      .firestore()
      .collection(dogsRef)
      .get()
      .then(snapshot => {
        let dogs = []
        snapshot.forEach(dog => {
          firebase
            .storage()
            .ref(`dogs/${dog.id}.png`)
            .getDownloadURL()
            .then(url => {
              const newDog = {
                id: dog.id,
                ...dog.data(),
                imageURL: url,
              }
              dogs.push(newDog)
            })
        })
        this.setState({ dogs })
        setTimeout(() => this.setState({ loading: false }), 1000)
      })
      .catch(err => console.error(err))
  }

  render() {
    const { classes } = this.props
    return (
      <Paper elevation={0} className={classes.paperDog}>
        <Grid
          container
          direction="column"
          // alignItems="center"
          justify="center"
          spacing={16}
        >
          <Grid item align="left">
            <Typography variant="h4" gutterBottom>
              Os nossos meninos
            </Typography>
          </Grid>
          <Grid
            container
            direction="column"
            // alignItems="center"
            // justify="center"
            spacing={0}
            className={classes.gridContainer}
          >
            {this.state.loading ? (
              <Grid item align="center">
                <CircularProgress color="secondary" />
              </Grid>
            ) : (
              <DogGrid dogs={this.state.dogs} />
            )}
          </Grid>
        </Grid>
      </Paper>
    )
  }
}

LandingGrid.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withFirebase(withStyles(styles)(LandingGrid))
