import React from "react"
import PropTypes from "prop-types"
import { withStyles } from "@material-ui/core/styles"
import { graphql, StaticQuery, Link } from "gatsby"

//Layout
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import Hidden from "@material-ui/core/Hidden"

import logo from "../../images/cropped-logo_sem_fundo.png"
import { theme } from "../../styles/muiTheme"

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
}

const TopMenu = props => {
  const { classes } = props

  return (
    <StaticQuery
      query={query}
      render={data => (
        <div className={classes.root}>
          <AppBar position="fixed">
            <Toolbar className={classes.logo} mdUp>
              <Hidden mdUp>
                <Link to="/" className={classes.grow}>
                  <img
                    src={logo}
                    width={65}
                    height={60}
                    style={{ margin: 5 }}
                    alt="logo"
                  />
                </Link>
              </Hidden>
              <Hidden smDown>
                <Link to="/">
                  <img
                    src={logo}
                    width={65}
                    height={60}
                    style={{ margin: 5 }}
                    alt="logo"
                  />
                </Link>
              </Hidden>
              <Hidden smDown>
                <Link
                  to="/"
                  style={{ textDecoration: "none" }}
                  className={classes.grow}
                >
                  <Typography variant="h5" color="inherit">
                    Querido Aconchego
                  </Typography>
                </Link>
              </Hidden>
              {data.allNavbarJson.edges.map((item, index) => (
                <Link
                  to={item.node.link}
                  style={{ textDecoration: "none" }}
                  activeStyle={{
                    color: theme.palette.secondary.light,
                  }}
                  key={index}
                >
                  <Button color="inherit">{item.node.label}</Button>
                </Link>
              ))}
            </Toolbar>
          </AppBar>
        </div>
      )}
    />
  )
}

const query = graphql`
  query NavbarItemsQuery {
    allNavbarJson {
      edges {
        node {
          label
          link
        }
      }
    }
  }
`

TopMenu.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(TopMenu)
