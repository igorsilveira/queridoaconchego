import React from "react"
import { withStyles } from "@material-ui/core/styles"
import CircularProgress from "@material-ui/core/CircularProgress"
import Grid from "@material-ui/core/Grid"

const styles = theme => ({
  root: {
    backgroundColor: "white",
    height: "100vh",
    width: "100wh",
    position: "absolute",
    top: 0,
  },
})

const CircularLoading = props => {
  const { classes } = props
  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <Grid item>
        <CircularProgress color="secondary" />
      </Grid>
    </Grid>
  )
}

export default withStyles(styles)(CircularLoading)
