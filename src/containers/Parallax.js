import React from "react"
import PropTypes from "prop-types"

// Layout
import Grid from "@material-ui/core/Grid"
import classNames from "classnames"
import Typography from "@material-ui/core/Typography"
import { withStyles } from "@material-ui/core/styles"
import Fab from "@material-ui/core/Fab"
import Paper from "@material-ui/core/Paper"

import dogImage from "../images/dog.jpg"
import lunaImage from "../images/Luna.jpg"

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    // paddingTop: theme.spacing.unit * 20,
    // margin: "auto",
    backgroundAttachment: "fixed",
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    // borderBottomRight: 20,
    borderRadius: "0% 0% 100% 0% / 0% 0% 21% 0% ",
  },
  dog: {
    backgroundImage: `url(${dogImage})`,
  },
  luna: {
    backgroundImage: `url(${lunaImage})`,
  },
  parallaxFull: {
    height: "100vh",
    paddingTop: "37vh",
  },
  parallaxHalf: {
    height: "60vh",
    paddingTop: "17vh",
  },
  gridContainer: {
    marginLeft: "20%",
    maxWidth: "50%",
  },
  button: {
    marginRight: theme.spacing.unit * 2,
    fontSize: 17,
    width: 250,
  },
})

const MyParallax = props => {
  const { classes, path, height, info } = props
  return (
    <div className={classes.root}>
      <Paper
        className={classNames(
          classes.paper,
          classes[path],
          height === "full" ? classes.parallaxFull : classes.parallaxHalf
        )}
        elevation={0}
        square
      >
        <Grid
          container
          spacing={0}
          align="flex-start"
          justify="center"
          direction="column"
          className={classes.gridContainer}
        >
          <Grid item xs>
            <Typography
              variant="h1"
              color="primary"
              align="left"
              className={classes.gridTexts}
              gutterBottom
            >
              {info.title}
            </Typography>
          </Grid>
          <Grid item xs>
            <Typography
              variant="h4"
              color="primary"
              align="left"
              className={classes.gridTexts}
              paragraph
              style={{ lineHeight: 2 }}
            >
              {info.description}
            </Typography>
          </Grid>
          <Grid item container align="flex-start" direction="row">
            {info.buttons.map((btn, index) => (
              <Grid item key={index}>
                <Fab
                  variant="extended"
                  aria-label={btn.ariaLabel}
                  color={index % 2 === 0 ? "secondary" : "primary"}
                  className={classes.button}
                >
                  {btn.label}
                </Fab>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Paper>
    </div>
  )
}

MyParallax.defaultProps = {
  path: "dog",
  height: "half",
}

MyParallax.propTypes = {
  path: PropTypes.string,
  height: PropTypes.string,
  // children: PropTypes.node.isRequired,
}

export default withStyles(styles)(MyParallax)
