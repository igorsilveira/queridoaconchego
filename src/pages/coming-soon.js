import React, { Component } from "react"
// import { connect } from "react-redux"
// import { graphql, StaticQuery } from "gatsby"
import axios from "axios"
import { withFirebase } from "../components/firebase/context"

import SEO from "../components/seo"
import Container from "../containers/container"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"

// Icons
import logo from "../images/cropped-logo_sem_fundo.png"
import LoadingSvg from "../images/svg/loading.svg"
import FacebookIcon from "../images/social/facebook.png"
import InstagramIcon from "../images/social/instagram.png"

// Layout
import { withStyles } from "@material-ui/core/styles"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Fab from "@material-ui/core/Fab"
import { Paper, CircularProgress, LinearProgress } from "@material-ui/core"

const logoDimensions = 100
const logoPadding = 20

// const mapStateToProps = ({ dogs }) => ({ dogs })

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundPosition: "top",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    backgroundImage: `url(${LoadingSvg})`,
  },
  paper: {
    marginTop: "8vh",
    marginBottom: "8vh",
    flexGrow: 1,
    padding: 20,
  },
  logo: {
    width: `${logoDimensions}px`,
    height: `${logoDimensions}px`,
  },
  logoWrapper: {
    display: "flex",
    // boxShadow: "inset 0px 0px 38px 0px rgba(0,0,0,0.3)",
    width: `${logoDimensions + logoPadding}px`,
    height: `${logoDimensions + logoPadding}px`,
    borderRadius: `${(logoDimensions + logoPadding) / 4}px`,
    border: `dashed 3px ${theme.palette.secondary.light}`,
    backgroundColor: "#fff",
  },
  svgLoading: {
    height: 400,
  },
  gridContainer: {
    // marginLeft: "20%",
    // maxWidth: "50%",
    marginTop: 50,
    marginBottom: 50,
  },
  title: {
    margin: 20,
  },
  fabList: {
    marginTop: 10,
    display: "flex",
  },
  fabIcon: {
    margin: "auto",
    width: 30,
    height: 30,
  },
  fab: {
    boxShadow: "none",
  },
  mcForm: {
    display: "none",
  },
})

class ComingSoonPage extends Component {
  render() {
    const { classes } = this.props

    return (
      <div className={classes.root}>
        <SEO
          title="Coming Soon"
          keywords={[`animal`, `shelter`, `querido aconchego`, `abrigo`]}
        />
        <Container>
          <Grid
            container
            spacing={16}
            align="center"
            justify="center"
            direction="column"
            className={classes.gridContainer}
          >
            <Grid item className={classes.logoWrapper}>
              <img src={logo} className={classes.logo}></img>
            </Grid>
            <Grid item>
              <Paper className={classes.paper} elevation={10}>
                <Grid
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Grid item>
                    <Typography
                      variant="h2"
                      className={classes.title}
                      color="secondary"
                    >
                      Bem vindo!
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="body1" paragraph color="secondary">
                      Brevemente, este será um espaço onde poderá ajudar a{" "}
                      <span style={{ fontWeight: "bold" }}>
                        Querido Aconchego Associação Animal de Felgueiras
                      </span>{" "}
                      e os seus patudos.
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" paragraph color="secondary">
                      Se quiser saber quando estiver tudo pronto junte-se à
                      nossa lista de email.
                    </Typography>
                    <Button
                      href="http://eepurl.com/gMXAQL"
                      variant="contained"
                      color="secondary"
                      size="large"
                    >
                      Inscreva-se
                    </Button>
                  </Grid>
                  <Grid item className={classes.fabList}>
                    <Grid container direction="row">
                      <Grid item>
                        <Fab
                          href="https://www.facebook.com/queridoaconchego/"
                          className={classes.fab}
                          color="primary"
                          size="small"
                        >
                          <img
                            className={classes.fabIcon}
                            src={FacebookIcon}
                          ></img>
                        </Fab>
                      </Grid>
                      <Grid item>
                        <Fab
                          href="https://www.instagram.com/queridoaconchegoaaf/"
                          className={classes.fab}
                          color="primary"
                          size="small"
                        >
                          <img
                            className={classes.fabIcon}
                            src={InstagramIcon}
                          ></img>
                        </Fab>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </div>
    )
  }
}

export default withFirebase(withStyles(styles)(ComingSoonPage))
