import React from "react"
import SEO from "../components/seo"

const ComoAjudar = () => {
  return (
    <>
      <SEO
        title="Como Ajudar"
        keywords={[
          `animal`,
          `abrigo`,
          `shelter`,
          `querido aconchego`,
          `ajudar`,
        ]}
      />
      ComoAjudar
    </>
  )
}

export default ComoAjudar
