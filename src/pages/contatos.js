import React from "react"
import SEO from "../components/seo"
import { graphql } from "gatsby"

const Contatos = ({ data }) => {
  return (
    <>
      <SEO
        title="Contatos"
        keywords={[
          `animal`,
          `abrigo`,
          `shelter`,
          `querido aconchego`,
          `contatos`,
        ]}
      />
      {data.site.siteMetadata.author}
    </>
  )
}

export const query = graphql`
  query {
    site {
      siteMetadata {
        author
      }
    }
  }
`

export default Contatos
