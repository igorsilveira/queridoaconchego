import React, { Component } from "react"
// import { connect } from "react-redux"
// import { graphql, StaticQuery } from "gatsby"
import { withFirebase } from "../components/firebase/context"

import LandingGrid from "../containers/Dogs/LandingGrid"
import MyParallax from "../containers/Parallax"
import SEO from "../components/seo"
import Container from "../containers/container"
import DoubleColor from "../containers/DoubleColor"

// Layout
import { withStyles } from "@material-ui/core/styles"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Fab from "@material-ui/core/Fab"

import dogImage from "../images/drei.jpg"

// const mapStateToProps = ({ dogs }) => ({ dogs })

const info = {
  title: "Adote um Amigo",
  description: "dasdasiudgasidabsdkjahsbdkas",
  buttons: [
    {
      label: "Adotar",
      ariaLabel: "adopt",
    },
    {
      label: "Doar",
      ariaLabel: "donate",
    },
  ],
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  backgroundColor: {
    marginTop: -40,
    backgroundColor: theme.palette.secondary.dark,
  },
  background: {
    // paddingTop: theme.spacing.unit * 20,
    // margin: "auto",
    // backgroundAttachment: "fixed",
    paddingBottom: 220,
    backgroundPosition: "top",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    // borderBottomRight: 20,
    borderRadius: "0% 0% 0% 100% / 0% 0% 0% 21%",
    backgroundImage: `url(${dogImage})`,
  },
  paper: {
    // paddingTop: theme.spacing.unit * 20,
    // margin: "auto",
    // backgroundAttachment: "fixed",
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    // borderBottomRight: 20,
    borderRadius: "0% 0% 100% 0% / 0% 0% 21% 0%",
  },
  gridContainer: {
    // marginLeft: "20%",
    // maxWidth: "50%",
    marginTop: 50,
    marginBottom: 50,
  },
  halfColorGrid: {
    padding: "50px 80px",
  },
  button: {
    marginRight: theme.spacing.unit * 2,
    fontSize: 17,
    // width: 250,
    // backgroundColor: theme.palette.fallback.main,
    backgroundColor: theme.palette.secondary,
    opacity: 0.8,
  },
  icon: {
    fontSize: 50,
  },
})

class IndexPage extends Component {
  render() {
    const { classes } = this.props
    return (
      <div>
        <SEO
          title="Home"
          keywords={[`animal`, `shelter`, `querido aconchego`]}
        />
        <MyParallax path="dog" height="half" info={info} />
        <Container>
          <Grid
            container
            spacing={16}
            align="center"
            justify="center"
            direction="column"
            // xs={10}
            className={classes.gridContainer}
          >
            {/* Titles Column */}
            <Grid container item direction="column" spacing={16}>
              <Grid item xs>
                <Typography variant="h6" color="textSecondary">
                  FAÇA PARTE DA FAMÍLIA
                </Typography>
              </Grid>
              <Grid item xs>
                <Typography variant="h2">Ajude um Amigo</Typography>
              </Grid>
            </Grid>
            {/* Row with call to action */}
            <Grid
              container
              item
              direction="row"
              spacing={32}
              className={classes.gridContainer}
            >
              <Grid item xs>
                <Grid
                  container
                  item
                  direction="column"
                  align="left"
                  spacing={16}
                >
                  <Grid item>
                    <Typography variant="h5">ADOTANDO UM AMIGO</Typography>
                  </Grid>
                  <Grid item xs>
                    <Typography variant="subtitle1" paragraph>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum
                      dolor sit amet, consectetur adipiscing elit. Nulla quis
                      lorem ut libero malesuada feugiat. Pellentesque in ipsum
                      id orci porta dapibus. Lorem ipsum dolor sit amet,
                      consectetur
                    </Typography>
                  </Grid>
                  <Grid item xs>
                    <Fab
                      variant="extended"
                      className={classes.button}
                      color="secondary"
                    >
                      OS AMIGOS
                    </Fab>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs>
                <Grid
                  container
                  item
                  direction="column"
                  align="left"
                  spacing={16}
                >
                  <Grid item>
                    <Typography variant="h5">FAZENDO UMA DOAÇÃO</Typography>
                  </Grid>
                  <Grid item xs>
                    <Typography variant="subtitle1" paragraph>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum
                      dolor sit amet, consectetur adipiscing elit. Nulla quis
                      lorem ut libero malesuada feugiat. Pellentesque in ipsum
                      id orci porta dapibus. Lorem ipsum dolor sit amet,
                      consectetur
                    </Typography>
                  </Grid>
                  <Grid item xs>
                    <Fab
                      variant="extended"
                      className={classes.button}
                      color="secondary"
                    >
                      DOAR
                    </Fab>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
        <div className={classes.backgroundColor}>
          <div className={classes.background}>
            <Container>
              <Grid container>
                <Grid item sm={12} md={7} lg={6}>
                  <LandingGrid />
                </Grid>
                <Grid item />
              </Grid>
            </Container>
          </div>
          <Container>
            <Grid
              container
              spacing={16}
              align="center"
              justify="center"
              direction="column"
              // xs={10}
              className={classes.gridContainer}
            >
              {/* Titles Column */}
              <Grid container item direction="column" spacing={16}>
                <Grid item xs>
                  <Typography variant="h4" color="primary">
                    Getting Involved
                  </Typography>
                </Grid>
                <Grid item xs>
                  <Typography variant="h2" color="primary">
                    Ajude um Animal
                  </Typography>
                </Grid>
              </Grid>
              {/* Row with call to action */}
              <Grid
                container
                item
                direction="row"
                spacing={40}
                className={classes.gridContainer}
              >
                <Grid item md={6} sm={12}>
                  <Grid
                    container
                    item
                    direction="column"
                    align="center"
                    spacing={16}
                  >
                    <Grid item>
                      <img
                        src="https://www.elegantthemes.com/layouts/wp-content/uploads/2018/06/paw.png"
                        alt="paw-icon"
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="h5" color="primary">
                        ADOPTING A PET
                      </Typography>
                    </Grid>
                    <Grid item xs>
                      <Typography variant="subtitle1" paragraph color="primary">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nulla quis lorem ut libero malesuada feugiat.
                        Pellentesque in ipsum id orci porta dapibus. Lorem ipsum
                        dolor sit amet, consectetur
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item md={6} sm={12}>
                  <Grid
                    container
                    item
                    direction="column"
                    align="center"
                    spacing={16}
                  >
                    <Grid item>
                      <img
                        src="https://www.elegantthemes.com/layouts/wp-content/uploads/2018/06/certificate.png"
                        alt="certificate-icon"
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="h5" color="primary">
                        ADOPTING A PET
                      </Typography>
                    </Grid>
                    <Grid item xs>
                      <Typography variant="subtitle1" paragraph color="primary">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nulla quis lorem ut libero malesuada feugiat.
                        Pellentesque in ipsum id orci porta dapibus. Lorem ipsum
                        dolor sit amet, consectetur
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item md={6} sm={12}>
                  <Grid
                    container
                    item
                    direction="column"
                    align="center"
                    spacing={16}
                  >
                    <Grid item>
                      <img
                        src="https://www.elegantthemes.com/layouts/wp-content/uploads/2018/06/house.png"
                        alt="house-icon"
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="h5" color="primary">
                        ADOPTING A PET
                      </Typography>
                    </Grid>
                    <Grid item xs>
                      <Typography variant="subtitle1" paragraph color="primary">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nulla quis lorem ut libero malesuada feugiat.
                        Pellentesque in ipsum id orci porta dapibus. Lorem ipsum
                        dolor sit amet, consectetur
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item md={6} sm={12}>
                  <Grid
                    container
                    item
                    direction="column"
                    align="center"
                    spacing={16}
                  >
                    <Grid item>
                      <img
                        src="https://www.elegantthemes.com/layouts/wp-content/uploads/2018/06/neuter.png"
                        alt="neuter-icon"
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="h5" color="primary">
                        ADOPTING A PET
                      </Typography>
                    </Grid>
                    <Grid item xs>
                      <Typography variant="subtitle1" paragraph color="primary">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nulla quis lorem ut libero malesuada feugiat.
                        Pellentesque in ipsum id orci porta dapibus. Lorem ipsum
                        dolor sit amet, consectetur
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Container>
        </div>
        <DoubleColor>
          <Grid
            container
            spacing={32}
            align="center"
            justify="center"
            direction="column"
            // xs={10}
            className={classes.halfColorGrid}
          >
            {/* Titles Column */}
            <Grid container item direction="column" spacing={16}>
              <Grid item xs>
                <Typography variant="h6" color="textSecondary">
                  O QUE NOS GUIA
                </Typography>
              </Grid>
              <Grid item xs>
                <Typography variant="h3" color="primary">
                  A Nossa Missão
                </Typography>
              </Grid>
            </Grid>
            {/* Row with call to action */}
            <Grid item xs>
              <Grid item xs>
                <Typography
                  variant="body1"
                  color="primary"
                  paragraph
                  style={{ opacity: 0.9 }}
                >
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit
                  amet, consectetur adipiscing elit. Nulla quis lorem ut libero
                  malesuada feugiat. Pellentesque in ipsum id orci porta
                  dapibus. Lorem ipsum dolor sit amet, consectetur
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </DoubleColor>
      </div>
    )
  }
}

export default withFirebase(withStyles(styles)(IndexPage))
