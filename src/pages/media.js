import React from "react"
import SEO from "../components/seo"
import { graphql } from "gatsby"

const MediaPage = ({ data }) => {
  return (
    <>
      <SEO
        title="Media"
        keywords={[
          `animal`,
          `abrigo`,
          `shelter`,
          `querido aconchego`,
          `media`,
          `galeria`,
        ]}
      />
      Media
    </>
  )
}

export const query = graphql`
  query {
    placeholderImage: file(relativePath: { eq: "dog.jpg" }) {
      absolutePath
    }
  }
`
export default MediaPage
