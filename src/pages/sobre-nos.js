import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"

const SobreNos = ({ data }) => {
  return (
    <>
      <SEO
        title="Sobre Nós"
        keywords={[
          `animal`,
          `abrigo`,
          `shelter`,
          `querido aconchego`,
          `ajudar`,
          `sobre`,
        ]}
      />
      {data.site.siteMetadata.title}
      {data.site.siteMetadata.responsible}
    </>
  )
}

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
export default SobreNos
