import React, { Component } from "react"
import SEO from "../components/seo"

//Wrappers
import { withFirebase } from "../components/firebase/context"
import { withStyles } from "@material-ui/core/styles"

// Components
import CircularLoading from "../containers/CircularLoading"
import Container from "../containers/container"
import DoubleColor from "../containers/DoubleColor"
import SimpleTabs from "../components/dogs/SimpleTabs"
import DogColumn from "../components/dogs/DogColumn"

import TextField from "@material-ui/core/TextField"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"

import * as Utils from "../utils/utils"
import { Button } from "@material-ui/core"

import axios from "axios"

const dogsRef = "dogs"

const styles = theme => ({
  img: {
    width: "100%",
    display: "block",
    height: "auto",
  },
  form: {
    // margin: 40,
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    boxShadow: "0px 10px 40px -16px rgba(0,0,0,0.18)",
  },
  backgroundColor: {
    marginTop: -40,
    backgroundColor: theme.palette.secondary.dark,
  },
  halfColorGrid: {
    padding: 40,
  },
  button: {
    marginTop: theme.spacing.unit,
    fontSize: 17,
    // width: 250,
    // backgroundColor: theme.palette.fallback.main,
    backgroundColor: theme.palette.secondary,
    opacity: 0.8,
  },
})

class Amigo extends Component {
  constructor(props) {
    super(props)

    this.state = {
      info: { general: null, about: null, history: null },
      loading: true,
      name: "",
      email: "",
      message: "",
      otherDogs: [],
      errors: {
        name: false,
        email: false,
        message: false,
      },
      initializing: true,
    }
  }

  async loadOthers() {
    const { firebase } = this.props
    const params = Utils.getJsonFromUrl(this.props.location.search)

    let otherDogs = []

    const querySnapshot = await firebase
      .firestore()
      .collection(dogsRef)
      .limit(3)
      .get()

    await querySnapshot.forEach(async dog => {
      // Less than 2 and different from the current
      if (otherDogs.length < 2 && dog.id !== params.id) {
        // Get dog image
        const url = await firebase
          .storage()
          .ref(`dogs/${dog.id}.png`)
          .getDownloadURL()

        otherDogs.push({ id: dog.id, ...dog.data(), url })

        this.setState({
          otherDogs,
          hasOthers: otherDogs.length > 0,
        })
      }
    })
  }

  async loadDog() {
    const { firebase } = this.props
    const params = Utils.getJsonFromUrl(this.props.location.search)

    const dog = await firebase
      .firestore()
      .collection(dogsRef)
      .doc(params.id)
      .get()

    const { name, age, breed, color, genre, health, about, size } = dog.data()

    const url = await firebase
      .storage()
      .ref(`dogs/${dog.id}.png`)
      .getDownloadURL()

    this.setState({
      info: {
        general: {
          Nome: name,
          Idade: age,
          Raça: breed,
          Cor: color,
          Género: genre,
          Saúde: health,
          Tamanho: size,
        },
        imageURL: url,
        about,
      },
    })
  }

  handleSendEmail = async event => {
    event.preventDefault()
    const {
      name,
      email,
      message,
      info: {
        general: { Nome: animal },
      },
    } = this.state
    const { firebase } = this.props

    if (!this.validateFormInputs()) {
      return
    }

    axios
      .post("https://querido-aconchego-messaging.herokuapp.com/email", {
        name,
        email,
        message,
        animal,
      })
      .then(sent => {
        return { message: "sent" }
      })
      .catch(error => {
        return { message: "error" }
      })
  }

  async loadInfo() {
    await this.loadDog()
    await this.loadOthers()

    this.setState({ loading: false })
  }

  componentDidMount() {
    const { firebase } = this.props
    this.getSendGridConfig = firebase
      .functions()
      .httpsCallable("getSendGridConfig")
    this.loadInfo()
  }

  validateFormInputs() {
    const { errors } = this.state
    return Object.keys(errors).every(e => !errors[e])
  }

  handleChange = name => event => {
    const { name: userName, email, message, initializing } = this.state
    if (initializing) {
      this.setState({ initializing: false })
    }
    const errors = {}

    errors.name = userName.length < 3
    errors.email = !Utils.validEmail(email)
    errors.message = message.length < 3 || message.length > 300

    this.setState({
      [name]: event.target.value,
      errors,
    })
  }

  render() {
    const {
      info: { general, about, history, imageURL },
      info,
      otherDogs,
      hasOthers,
      name,
      email,
      message,
      errors,
      initializing,
    } = this.state

    const { classes } = this.props

    this.validateFormInputs()

    return (
      <>
        <SEO
          title="Amigo"
          keywords={[
            `animal`,
            `abrigo`,
            `shelter`,
            `querido aconchego`,
            `amigo`,
            `adotar`,
            `adopt`,
          ]}
        />
        {this.state.loading ? (
          <CircularLoading />
        ) : (
          // <div style={{ marginTop: 150 }}>{JSON.stringify(this.state.dog)}</div>
          <div>
            <Container>
              <main style={{ marginTop: "15vh" }}>
                <Typography variant="h6" color="textSecondary" gutterBottom>
                  {general.Raça.toUpperCase()}
                </Typography>
                <Typography variant="h2" gutterBottom>
                  {general.Nome}
                </Typography>
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  spacing={40}
                >
                  <Grid item sm={4}>
                    <img className={classes.img} alt="complex" src={imageURL} />
                  </Grid>
                  <Grid item container direction="column" sm={8}>
                    <Grid item style={{ marginBottom: 30 }}>
                      <SimpleTabs info={info} />
                    </Grid>
                    <Grid item>
                      <form
                        noValidate
                        className={classes.form}
                        onSubmit={this.handleSendEmail}
                      >
                        <Grid item container direction="column">
                          <Typography variant="h4" gutterBottom>
                            Venha conhecer{" "}
                            {general.Género === "Menina" ? `a` : `o`}{" "}
                            {general.Nome}
                          </Typography>
                          <Grid
                            item
                            container
                            direction="row"
                            justify="space-between"
                            spacing={16}
                          >
                            <Grid item sm={12} md={6}>
                              <TextField
                                id="name"
                                label="Nome"
                                fullWidth
                                error={errors.name}
                                className={classes.textField}
                                value={name}
                                name="name"
                                onChange={this.handleChange("name")}
                                margin="normal"
                                variant="filled"
                                InputProps={{
                                  disableUnderline: true,
                                }}
                              />
                            </Grid>
                            <Grid item sm={12} md={6}>
                              <TextField
                                id="email"
                                label="Email"
                                fullWidth
                                error={errors.email}
                                className={classes.textField}
                                value={email}
                                type="email"
                                name="email"
                                onChange={this.handleChange("email")}
                                margin="normal"
                                variant="filled"
                                InputProps={{
                                  disableUnderline: true,
                                }}
                              />
                            </Grid>
                          </Grid>
                          <Grid item>
                            <TextField
                              id="message"
                              multiline
                              label="Mensagem"
                              name="message"
                              error={errors.message}
                              className={classes.textField}
                              value={message}
                              rows="4"
                              fullWidth
                              onChange={this.handleChange("message")}
                              margin="normal"
                              variant="filled"
                              InputProps={{
                                disableUnderline: true,
                              }}
                            />
                          </Grid>
                        </Grid>
                        <Grid
                          container
                          direction="row"
                          justify="flex-end"
                          spacing={16}
                        >
                          <Grid item>
                            <Button
                              disabled={
                                !this.validateFormInputs() || initializing
                              }
                              type="submit"
                              variant="contained"
                              color="primary"
                              className={classes.button}
                              color="secondary"
                            >
                              ENVIAR
                            </Button>
                          </Grid>
                        </Grid>
                      </form>
                    </Grid>
                  </Grid>
                </Grid>
              </main>
            </Container>
            <DoubleColor inverted>
              <Grid
                container
                spacing={32}
                align="center"
                justify="center"
                direction="column"
                // xs={10}
                className={classes.halfColorGrid}
              >
                {/* Titles Column */}
                <Grid container item direction="column" spacing={16}>
                  <Grid item xs>
                    <Typography variant="h3" color="primary">
                      A Nossa Missão
                    </Typography>
                  </Grid>
                </Grid>
                {/* Row with call to action */}
                <Grid item xs>
                  <Grid item xs>
                    <Typography
                      variant="body1"
                      color="primary"
                      paragraph
                      style={{ opacity: 0.9 }}
                    >
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum
                      dolor sit amet, consectetur adipiscing elit. Nulla quis
                      lorem ut libero malesuada feugiat. Pellentesque in ipsum
                      id orci porta dapibus. Lorem ipsum dolor sit amet,
                      consectetur
                    </Typography>
                  </Grid>
                </Grid>
                <Grid item xs>
                  <Typography variant="h6" color="textSecondary">
                    COM MUITO AMOR, {general.Nome.toUpperCase()}
                  </Typography>
                </Grid>
              </Grid>
            </DoubleColor>
            <div className={classes.backgroundColor} style={{ paddingTop: 30 }}>
              <Container>
                <Grid
                  container
                  spacing={16}
                  align="center"
                  justify="center"
                  direction="column"
                  // xs={10}
                  className={classes.gridContainer}
                >
                  {hasOthers && (
                    <>
                      {/* Titles Column */}
                      <Grid container item direction="column" spacing={16}>
                        <Grid item xs>
                          <Typography variant="h6" color="primary">
                            FAÇA PARTE DA FAMÍLIA
                          </Typography>
                        </Grid>
                        <Grid item xs>
                          <Typography variant="h2" color="primary" gutterBottom>
                            Veja mais Amigos
                          </Typography>
                          <br />
                        </Grid>
                      </Grid>
                      {/* Row with call to action */}

                      <Grid
                        container
                        item
                        direction="row"
                        spacing={40}
                        className={classes.gridContainer}
                      >
                        <Grid item sm={12} md={6}>
                          <DogColumn {...otherDogs[0]} />
                        </Grid>
                        <Grid item sm={12} md={6}>
                          <DogColumn {...otherDogs[1]} />
                        </Grid>
                      </Grid>
                    </>
                  )}
                </Grid>
              </Container>
            </div>
          </div>
        )}
      </>
    )
  }
}

export default withFirebase(withStyles(styles)(Amigo))
