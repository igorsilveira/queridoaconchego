const functions = require("firebase-functions")
const axios = require("axios").default
const cors = require("cors")({
  origin: "https://querido-aconchego.firebaseapp.com",
})

exports.getSendGridConfig = functions.https.onCall((data, context) => {
  return functions.config().sendgrid
})
